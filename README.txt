This module is an expansion of the already existing Mailchimp module.

Mailchimp offers no permissions system when you login, so everyone has access
to any list or template.  This module allows you to grant users access to
specific lists and templates, and allows them to track campaign stats.

This module allows an administrator to assign Mailchimp lists and Mailchimp
Templates to users.
From those lists users can create new campaigns, author emails, preview emails,
send test emails and distribute the emails to any list they have been granted
access.After a campaign has been sent the user can review rudimentary campaign
stats.
